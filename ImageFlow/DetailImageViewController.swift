//
//  DetailImageViewController.swift
//  ImageFlow
//
//  Created by Admin on 30.08.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit
import Firebase

class DetailImageViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var doubleTabGesture: UITapGestureRecognizer!
    @IBOutlet weak var viewInScrollView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var post: FIRDataSnapshot!
    var ref: FIRDatabaseReference!
    private var _refHandle: FIRDatabaseHandle!
    var interstitial: GADInterstitial!
    
    var isFavorite: Bool = false
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doubleTabGesture.numberOfTapsRequired = 2
        ref = FIRDatabase.database().reference()
        imageView.image = image
        let postDict = post.value as! Dictionary<String, String>
        navigationItem.title = "by: \(postDict["userName"]!)"
        
        _refHandle = ref.child("favorite/\(FIRAuth.auth()!.currentUser!.uid)/\(post.key)").observeEventType(.Value, withBlock: { (snapshot) in
            if (snapshot.value as? Int) != nil {
                self.isFavorite = true
            }
        })
        createAndLoadInterstitial()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(showAd(_:)), userInfo: nil, repeats: true)
    }
    
    private func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4721072971453755/4002936427")
        let request = GADRequest()
        
        request.testDevices = [ kGADSimulatorID ]
        interstitial.loadRequest(request)
    }
    
    func showAd(timer: NSTimer) {
        if interstitial.hasBeenUsed {
            timer.invalidate()
        }
        if interstitial.isReady {
            interstitial.presentFromRootViewController(self)
            timer.invalidate()
        }
    }

    @IBAction func didTapAddToFavorite(sender: AnyObject) {
     
        let alertController = UIAlertController(title: "Action sheet", message: "", preferredStyle: .ActionSheet)
        let alertCancleAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let alertShareImageAction = UIAlertAction(title: "Share image", style: .Default) { (action) in
            let activityController = UIActivityViewController(activityItems: [self.imageView.image!], applicationActivities: nil) // in [] can push anything like string, image and so one
            activityController.popoverPresentationController?.sourceView = self.view //so that’s ipad won’t crash
            self.presentViewController(activityController, animated: true, completion: nil)
            
        }
        let addToFavoriteText: String
        if isFavorite {
          addToFavoriteText = "Delete from favorite"
        } else {
          addToFavoriteText = "Add to favorite"
        }

        let alertAddToFavoriteAction = UIAlertAction(title: addToFavoriteText, style: .Default) { (action) in
            self.addToFavorite()
        }
        alertController.addAction(alertCancleAction)
        alertController.addAction(alertShareImageAction)
        alertController.addAction(alertAddToFavoriteAction)
        alertController.popoverPresentationController?.sourceView = self.view
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func addToFavorite() {
        if isFavorite {
            ref.child("favorite/\(FIRAuth.auth()!.currentUser!.uid)/\(post.key)").removeValue()
            isFavorite = false
            alertMessage("Image have been added to favorite")
        } else {
            ref.child("favorite/\(FIRAuth.auth()!.currentUser!.uid)/\(post.key)").setValue(true)
            isFavorite = true
            alertMessage("Image have been deleted from favorite")
        }
    }
    
    func alertMessage(message: String) {
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: .Alert)
        let alertCancelAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alertController.addAction(alertCancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return viewInScrollView
    }
    
    @IBAction func didDoubleTabForZoom(sender: AnyObject) {
        if scrollView.zoomScale != 1.0 {
            scrollView.zoomScale = 1.0
        } else {
            scrollView.zoomScale = 4.0
        }
    }

}
