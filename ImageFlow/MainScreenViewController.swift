//
//  MainScreenViewController.swift
//  ImageFlow
//
//  Created by Admin on 30.08.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class MainScreenViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
 
    var storageRef: FIRStorageReference!
    var ref: FIRDatabaseReference!
    var imageDataForDownload: NSData!
    var posts: [FIRDataSnapshot] = []
    private var _refHandle: FIRDatabaseHandle!
    
    private let leftAndRightPaddings: CGFloat = 20.0
    private let numberOfItemsPerRow: CGFloat = 4.0
    private let heightAdjustment: CGFloat = 0.8
    
    deinit {
        self.ref.child("posts").removeObserverWithHandle(_refHandle)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let width = (CGRectGetWidth(collectionView!.frame) - leftAndRightPaddings) / numberOfItemsPerRow
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSizeMake(width, width * heightAdjustment)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storageRef = FIRStorage.storage().referenceForURL("gs://image-flow.appspot.com")
        ref = FIRDatabase.database().reference()
        _refHandle = ref.child("posts").queryOrderedByChild("dataShtamp").queryLimitedToLast(40).observeEventType(.ChildAdded, withBlock: { (snapshop) in
            self.posts.append(snapshop)
            self.collectionView.insertItemsAtIndexPaths([NSIndexPath(forRow: self.posts.count-1, inSection: 0)])
        })
    }

    @IBAction func didImagePickerCameraAction(sender: AnyObject) {
        imagePickerControllerLoad(.Camera)
    }
    
    @IBAction func didImagePickerAction(sender: AnyObject) {
       imagePickerControllerLoad(.PhotoLibrary)
    }
    
    func imagePickerControllerLoad(sourseType: UIImagePickerControllerSourceType)
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = sourseType
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func didTapSendPostAction(sender: AnyObject) {
        if imageDataForDownload != nil {
            var photoUrl: String
            let key = ref.child("posts").childByAutoId().key
            let imageRef = storageRef.child("\(key).jpg")
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            imageRef.putData(imageDataForDownload, metadata: metaData, completion: { (metaData, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                self.collectionView.reloadItemsAtIndexPaths([NSIndexPath(forRow: self.posts.count-1, inSection: 0)])
            })
            photoUrl = imageRef.fullPath
            let userName: String
            if let user = FIRAuth.auth()!.currentUser!.displayName {
                userName =  user
            } else {
                userName = "Anonim"
            }
            let post = ["userName" : "\(userName)",
                        "photoUrl" : "\(photoUrl)",
                        "dataShtamp" : "\(NSDate().timeIntervalSince1970)"
            ]
            let childUpdates = ["/posts/\(key)" : post]
            ref.updateChildValues(childUpdates) { (error, ref) in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
        } else {
            alertMessage("You have not added any images!")
        }
    }
    
    @IBAction func didTapLogOutAction(sender: AnyObject) {
        do {
            try FIRAuth.auth()?.signOut()
            let fbLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            performSegueWithIdentifier("fromMainScreenToLogIn", sender: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let imageLength = UIImageJPEGRepresentation(image, 1)?.length where imageLength > 1_000_000 {
                imageDataForDownload = UIImageJPEGRepresentation(image, CGFloat(1_000_000.0 / Float(imageLength)))
            } else {
                imageDataForDownload = UIImageJPEGRepresentation(image, 1)
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toDetailView" {
             let destinationController = segue.destinationViewController as! DetailImageViewController
            let indexPath = collectionView.indexPathsForSelectedItems()
            let cell =  collectionView.cellForItemAtIndexPath(indexPath![0]) as! CollectionViewCell
            destinationController.image = cell.image!.image!
            destinationController.post = posts[indexPath![0].row]
        }
    }
}

extension MainScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("mailCell", forIndexPath: indexPath) as! CollectionViewCell
        let post = posts[indexPath.row].value as! Dictionary<String, String>
        if let photoUrl = post["photoUrl"] {
            FIRStorage.storage().referenceForURL("gs://image-flow.appspot.com/\(photoUrl)").dataWithMaxSize(INT64_MAX){ (data, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                    cell.image!.image = UIImage.init(data: data!)
                }
            }
        return cell
    }
    
    func alertMessage(message: String) {
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: .Alert)
        let alertCancelAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alertController.addAction(alertCancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
}
