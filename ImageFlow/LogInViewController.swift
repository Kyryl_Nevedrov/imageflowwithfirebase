//
//  LogInViewController.swift
//  ImageFlow
//
//  Created by Admin on 30.08.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class LogInViewController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loginButton = FBSDKLoginButton()
        let loginButtonLeftConstrain = NSLayoutConstraint(item: loginButton, attribute: .Trailing, relatedBy: .Equal, toItem: self.view, attribute: .Trailing, multiplier: 1.0, constant: -20)
        let loginButtonRightConstrain = NSLayoutConstraint(item: loginButton, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .Leading, multiplier: 1.0, constant: 20)
        let loginButtonBottomConstrain = NSLayoutConstraint(item: loginButton, attribute: .Bottom, relatedBy: .Equal, toItem: emailTextField, attribute: .Top, multiplier: 1.0, constant: -8)
        loginButton.readPermissions = ["public_profile", "email"]
        loginButton.center = self.view.center
        loginButton.delegate = self
        self.view.addSubview(loginButton)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activateConstraints([loginButtonLeftConstrain,loginButtonBottomConstrain,loginButtonRightConstrain])
   }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if FIRAuth.auth()?.currentUser != nil {
           logIn()
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        let credential = FIRFacebookAuthProvider.credentialWithAccessToken(FBSDKAccessToken.currentAccessToken().tokenString)
        FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
            if error == nil {
                self.logIn()
            } else {
                self.alertMessage(error!.localizedDescription)
            }
        }
    }
    
    @IBAction func logInAction(sender: AnyObject) {
        let email = emailTextField.text
        let pass = passTextField.text
        FIRAuth.auth()?.signInWithEmail(email!, password: pass!, completion: { (user, error) in
            if error == nil {
                self.logIn()
            } else {
                self.alertMessage(error!.localizedDescription)
            }
        })
    }
    
    @IBAction func signUpAction(sender: AnyObject) {
        let email = emailTextField.text
        let pass = passTextField.text
        FIRAuth.auth()?.createUserWithEmail(email!, password: pass!, completion: { (user, error) in
            if error == nil {
                let profileChangeRequest = user!.profileChangeRequest()
                profileChangeRequest.displayName = user!.email!.componentsSeparatedByString("@")[0]
                profileChangeRequest.commitChangesWithCompletion({ (error) in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                })
                self.logIn()
            } else {
                self.alertMessage(error!.localizedDescription)
            }
        })
    }

    @IBAction func forgotPasswordAction(sender: AnyObject) {
        let email = emailTextField.text
        FIRAuth.auth()?.sendPasswordResetWithEmail(email!, completion: { (error) in
            if error == nil {
                let alert = UIAlertController(title: "Message", message: "Password reset email sent", preferredStyle: .Alert)
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                self.alertMessage(error!.localizedDescription)
            }
        })
    }
    
    func logIn() {
        performSegueWithIdentifier("fromLogInToMainScreen", sender: nil)
    }
    
    func alertMessage(message: String) {
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: .Alert)
        let alertCancelAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alertController.addAction(alertCancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
  
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
    }

}
